\documentclass[11pt, a4paper]{article}
\usepackage{jheppub}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{cleveref}
\usepackage[nolist]{acronym}

\crefname{figure}{figure}{figures} % JHEP: figure should not be abbreviated. Do this instead of noabbrev package option to keep eq. abbreviations.

\graphicspath{{img/}}

\newcommand{\dd}{\text{d}}
\renewcommand{\i}{\ensuremath{\mathrm{i}}}
\newcommand{\eps}{\epsilon}
\newcommand{\re}{\mathrm{Re}}
\newcommand{\im}{\mathrm{Im}}

\newcommand{\lref}{\labelcref}
\newcommand{\incite}[1]{ref.~\cite{#1}}
\newcommand{\incites}[1]{refs.~\cite{#1}}

\newcommand{\RM}[1]{\textcolor{cyan}{{\bf\tt [RM: #1]}}}
\newcommand{\SZ}[1]{\textcolor{red}{{\bf\tt [SZ: #1]}}}
\newcommand{\FC}[1]{\textcolor{blue}{{\bf\tt [FC: #1]}}}

\title{Learning Feynman integrals from differential equations with neural networks}

\author[a]{Francesco Calisto,}
\author[b]{Ryan Moodie,}
\author[c]{Simone Zoia}

\affiliation[a]{
    Ludwig-Maximilians-Universit\"at, Theresienstraße 37, 80333 M\"unchen, Germany
}
\affiliation[b]{
    Dipartimento di Fisica, Università di Torino, and INFN, Sezione di Torino, Via P.\ Giuria 1, I-10125 Torino, Italy
}
\affiliation[c]{
    CERN, Theoretical Physics Department, CH-1211 Geneva 23, Switzerland
}

\emailAdd{ryaniain.moodie@unito.it}
\emailAdd{simone.zoia@cern.ch}

\abstract{
    \ldots
}

\begin{document}

\maketitle
\flushbottom

\begin{acronym}
    \acro{AD}{automatic differentiation}
    \acro{DE}{differential equation}
    \acro{GELU}{gaussian error linear unit}
    \acro{MPL}{multiple polylogarithm}
    \acro{MSE}{mean squared error}
    \acro{NN}{neural network}
    \acro{PINN}{physics-informed neural network}
    \acro{IBP}{integration-by-parts}
    \acro{MI}{master integral}
\end{acronym}


\section{Introduction}
\label{sec:Introduction}

The importance of Feynman integrals in theoretical physics can hardly be overstated.
They play a central role in quantum field theory, where they are necessary to obtain precise predictions for collider phenomenology and to unveil fundamental properties of the theory, but also find application in a growing range of other fields, such as gravitational waves, cosmology and statistical mechanics. \FC{Should we add references for these? If so I can suggest some and add them - I don't know about applications to statistical mechanics tough}
Their analytic expressions sport an intriguing `bestiary' of special functions, which has sparked the interest of mathematicians and has led to a fruitful cross-contamination between mathematics and physics.


The interest for Feynman integrals is therefore growing, and so is the effort in the search for techniques for evaluating them.
One of the most powerful approaches is the method of \acp{DE}~\cite{Barucchi:1973zm,Kotikov:1990kg,Kotikov:1991hm,Gehrmann:1999as,Bern:1993kr,Henn:2013pwa}, in which Feynman integrals are viewed as solutions to certain \acp{DE}. Solving these \acp{DE} analytically in terms of well-understood classes of special functions guarantees efficient numerical evaluation and complete analytical control, but is not always feasible.
In such cases, one may resort to a numerical solution of the \acp{DE}. In particular, the numerical solution through series expansions is gaining increasing interest after this technique was generalised from the univariate~\cite{Pozzorini:2005ff,Aglietti:2007as,Lee:2017qql,Lee:2018ojn,Bonciani:2018uvv,Fael:2021kyg,Fael:2022rgm} to the multivariate case~\cite{Liu:2017jxz,Moriello:2019yhu,Hidding:2020ytt,Liu:2021wks,Liu:2022chg,Armadillo:2022ugh}.
The most widely used fully numerical method is Monte Carlo integration with sector decomposition~\cite{Binoth:2000ps,Bogner:2007cr,Kaneko:2009qx,Borowka:2012yc,Borowka:2015mxa,Borowka:2017idc,Borowka:2018goh,Heinrich:2023til,Smirnov:2008py,Smirnov:2009pb,Smirnov:2013eza,Smirnov:2015mct,Smirnov:2021rhf}.
In addition to these, the long interest for Feynman integrals has led to a wide collection of other methods, such as Mellin-Barnes representation~\cite{Usyukina:1992jd,Usyukina:1993ch,Smirnov:1999gc,Tausk:1999vh,Czakon:2005rk,Smirnov:2009up,Gluza:2007rt,Belitsky:2022gba}, 
recurrence relations~\cite{Tarasov:1996br,Lee:2009dh,Lee:2012te},
symbolic integration~\cite{Brown:2008um,Panzer:2014caa}, 
loop-tree duality~\cite{Catani:2008xa,Runkel:2019yrs,Capatti:2019ypt}, 
Monte Carlo integration with tropical sampling~\cite{Borinsky:2020rqs,Borinsky:2023jdv}, 
and positivity constraints~\cite{Zeng:2023jek}.
We refer the interested readers to the comprehensive reviews~\cite{Smirnov:2012gma,Weinzierl:2022eaz}.
Recently, a few studies are beginning to explore the possibility of employing machine-learning methods in the evaluation Feynman integrals.
In \incite{Winterhalder:2021ngy}, the authors use a \ac{NN} to optimise the choice of integration contour to optimise the Monte Carlo integration with sector decomposition.
Another approach consists in training a \ac{NN} to learn the primitive of an integral, then replacing the Monte Carlo integration with evaluations of the primitive~\cite{Maitre:2022xle}.
A compendium of machine-learning applications in particle physics is available in \incites{Feickert:2021ajf,hepmllivingreview}.

In this paper we propose a new avenue: using machine learning to solve the \acp{DE} numerically.
We train a deep \ac{NN} to approximate the solution to the \acp{DE} by minimising a loss function that includes the \acp{DE} themselves along with a set of boundary values.
We build on the recently-proposed framework of \acp{PINN}~\cite{raissi2017physicsI,raissi2017physicsII,raissi2019physics}.
In contrast to the traditional machine learning regime, where one trains on a large dataset, here we use a small dataset ---~the boundary values for the \acp{DE}~--- and strong physical constraints in the form of \acp{DE}.
A preliminary study of this method was carried out by one of the authors in his bachelor's thesis project~\cite{thesis-calisto}.
\SZ{is it available anywhere?}
\RM{if not, I suggest zenodo}
\FC{It's not available now, but I can upload it to zenodo if you think it's better and appropriate}
This approach is very flexible as it does not rely on a canonical form of the \acp{DE}~\cite{Henn:2013pwa}, unlike the analytical techniques, and after training yields essentially instantaneous evaluation times, unlike the numerical methods, at the cost of a lower control over the accuracy.


In addition to discussing our new approach, we provide a proof-of-concept implementation~\cite{repo} within the \texttt{PyTorch} framework~\cite{paszke2019pytorch}, along with a number of one- and two-loop examples, some of them at the cutting edge of our current computational capabilities.
For these we achieved relative accuracies of 0.01--1\% \RM{update as required} in the physical phase space with training times in the order of an hour on a laptop.

%Approximation of squared amplitudes~\cite{Badger:2020uow,Aylett-Bullock:2021hmo,Moodie:2022flt,Badger:2022hwf}.
%And using IR~\cite{Maitre:2021uaa,Truong:2023rkt,Maitre:2023dqz}.

\medskip

This article is organised as follows. \SZ{update when complete}



\section{Differential equations for Feynman integrals}

In this section we give a quick overview of the method of \acp{DE} for computing Feynman integrals~\cite{Barucchi:1973zm,Kotikov:1990kg,Kotikov:1991hm,Gehrmann:1999as,Bern:1993kr,Henn:2013pwa}.
In order to make the discussion more concrete, we will use the one-loop four-point ``massless box'' Feynman integrals as a running example.
We refer to \incite{Henn:2014qga,Badger:2023eqz} for a detailed discussion. 

\SZ{maybe anticipate here the general form of the \acp{DE}, then two subsections: box example and general case}

The first step is to define the integral family of interest. 
An integral family consists in the set of all scalar integrals with a given propagator structure. 
The integrals of the massless box family are defined as\footnote{We omit Feynman's prescription for the infinitesimal imaginary parts of the propagators. See e.g.\ \cite{Badger:2023eqz}.}
\begin{align}
\mathrm{I}_{\vec{a}}(s,t ; \eps) = \int \frac{\dd^D k}{\i \pi^{D/2}} \frac{1}{D_1^{a_1} D_2^{a_2} D_3^{a_3} D_4^{a_4}} \,,
\end{align}
where $D = 4 - 2 \epsilon$ is the space-time dimension, $\vec{a} = (a_1,a_2,a_3,a_4) \in \mathbb{Z}^4$, and $D_i$ are the inverse propagators associated with the graph in \cref{fig:box} \SZ{figure}
\begin{align}
D_1 = - k^2 \,, \quad D_2  = - (k+p_1)^2 \,, \quad D_3 = -(k+p_1+p_2)^2 \,, \quad D_4 = - (k-p_4)^2 \,.
\end{align}
The external momenta $p_i$ are taken to be outgoing, and satisfy the following momentum-conservation and on-shell conditions:
\begin{align}
p_1 + p_2 + p_3 + p_4 = 0 \,, \qquad \qquad p_i^2 = 0 \ \forall \, i = 1,\ldots, 4 \,.
\end{align}
As a result, the box integrals depend on two independent Lorentz invariants only. We choose them as $s = (p_1+p_2)^2$ and $t = (p_2+p_3)^2$.

Within each family only finitely many integrals are linearly independent. 
These are called \acp{MI} in the literature, and act as a basis of the family. 
In other words, any integral of the family can be ``reduced'' to a linear combination of \acp{MI}, with coefficients which are rational functions of the kinematic invariants and $\epsilon$.\footnote{One may however introduce algebraic factors, such as square roots, in the definition of the \acp{MI}.}
The standard way to determine the \acp{MI} and perform the reductions to \acp{MI} is to solve systems of  \ac{IBP} relations~\cite{Tkachov:1981wb,Chetyrkin:1981qh} ---~linear relations among the integrals of a family~--- using the Laporta algorithm~\cite{Laporta:2000dsw}. In this work we generate the required \ac{IBP} relations using \texttt{LiteRed}~\cite{Lee:2012cn}, and solve them using \texttt{FiniteFlow}'s linear solver. The box family has three master integrals, which the Laporta algorithm chooses as
\begin{align}
\vec{F}(s,t ; \epsilon) = \begin{pmatrix}
\mathrm{I}_{0,1,0,1}(s,t ; \eps) \\
\mathrm{I}_{1,0,1,0}(s,t ; \eps) \\
\mathrm{I}_{1,1,1,1}(s,t ; \eps)
\end{pmatrix} \,.
\end{align}

Now the \acp{DE} come into play.
The derivatives of the \acp{MI} with respect to the kinematic invariants can be expressed as linear combinations of scalar integrals in the same family, and can thus be \ac{IBP}-reduced to \acp{MI} themselves. As a result, we obtain a system of first-order partial \acp{DE}:
\begin{align}
%\begin{aligned}
\frac{\partial}{\partial s} \vec{F}(s,t ; \epsilon)  = 
  \begin{pmatrix}
  0 & 0 & 0 \\ 
  0 & -\frac{\eps}{s} & 0 \\
  \frac{2 (2 \eps-1)}{s t (s + t)} &  \frac{2 (1 - 2 \eps)}{s^2 (s + t)} & -\frac{s + t + \eps \, t}{s (s + t)} \\ 
  \end{pmatrix} \cdot \vec{F}(s,t ; \epsilon) \,, 
%\\
%\frac{\partial}{\partial t} \vec{F}(s,t ; \epsilon)  = 
% \begin{pmatrix}
%  -\frac{\eps}{t} & 0 & 0 \\
%    0 & 0 & 0 \\
%    \frac{2 (1 - 2 \eps)}{t^2 (s + t)} & \frac{2 (2 \eps -1)}{s t (s + t)} & 
%      -\frac{s + t + \eps \, s}{t (s + t)} \\
%  \end{pmatrix} \cdot \vec{F}(s,t ; \epsilon) \,.  
%\end{aligned}
\end{align}
and similarly for $t$.

The goal then is to solve these \acp{DE}. In particular, we are interested in the Laurent expansion of the \acp{MI} around $\eps = 0$,
\begin{align}
\vec{F}(s,t; \eps) = \frac{1}{\eps^2} \sum_{w \ge 0} \eps^w \, \vec{F}^{(w)}(s,t) \,.
\end{align}
In practice, we are only interested in the solution truncated at a certain order in $\eps$, depending on the application.

\SZ{work in progress}

\SZ{things to mention: singularities (physical vs.\ spurious)}
\SZ{perhaps do two subsections: the box and the general case}

\acp{DE} in canonical form~\cite{Henn:2013pwa}

% Auxiliary mass flow~\cite{Liu:2017jxz,Liu:2021wks} (\textsc{AMFlow}~\cite{Liu:2022chg}).


\RM{Explain $\mu$ dependence.}

\SZ{MPLs -> under control. More complicated functions -> mathematical technology not ripe despite huge effort. Furthermore, even in those cases where an analytical expression in terms of elliptic functions is obtained, its numerical evaluation often remains challenging. Particularly problematic for pheno applications.}

We construct the problem as a first-order system of $n$ partial \acp{DE}.
Its solutions are dimensionless functions $I_i(\vec s; \eps)$ for $i=1,\ldots,n$, which depend on a set of $m$ linearly independent kinematic variables $\vec s$ and the dimensional regulator $\eps$.
We write the \acp{DE} in terms of connection matrices $A_q$ for $q\in\vec s$,
\begin{align}
    \label{eq:deq}
    0 &=\partial_q I_i(\vec s;\eps) - \sum_{j=1}^{n} A_{q,ij}(\vec s; \eps) \, I_j(\vec s; \eps) \,.
\end{align}
We also know at least one set of values $\{I_i(\vec s_0; \eps)\}$, which serves as the boundary condition.

We treat the functions and connection matrices as truncated series in $\eps$,
\begin{align}
    \label{eq:eps-series}
    I_i(\vec s;\eps) &= \sum_{w=w_0}^{w_1} I_i^{(w)}(\vec s)\, \eps^w + \mathcal{O}(\eps^{w_1+1}) \,, \\
    A_q(\vec s;\eps) &= \sum_{k=0}^{k_1} A_q^{(k)}(\vec s)\, \eps^k + \mathcal{O}(\eps^{k_1+1}) \,,
\end{align}
where the functions start at $\eps$ order $w_0$ and include $\nu$ orders such that $w_1=w_0+\nu$, and the connection matrices start at $\eps$ order zero and have $k_1\le \nu$ orders in $\eps$.
\RM{comment on smaller $k_1$ better}
The $\eps$ coefficients of the functions $I_i^{(w)}$ are complex-valued.
The elements of the $\eps$ coefficients of the connection matrices $A_{q,ij}^{(k)}$ are real-valued rational functions.
We write \cref{eq:deq} order by order,
\begin{align}
    \label{eq:de-eps}
    0 &= \partial_q I_i^{(w)}(\vec s) - \sum_{k=0}^{\min(k_1,w)} \sum_{j=1}^{n} A_{q,ij}^{(k)}(\vec s) \, I_j^{(w-k)}(\vec s) \,.
\end{align}

The \acp{DE} for the real and imaginary parts of the functions $I_i^{(w)}(\vec s)$ form two decoupled systems.
Therefore, they can be treated independently.

It remains to solve for $I_i^{(w)}(\vec s)$.

\section{Regression by machine learning}

In brief, \acp{NN} are trained by minimising a heuristic called a loss function, typically using a variant of stochastic gradient descent~\cite{Goodfellow-et-al-2016}.
By evaluating the loss function then considering its derivatives with respect to the parameters of the \ac{NN}, we can update those parameters to reduce the value of the loss function.
Repeating this iteratively, we converge on a minimum of the loss function.
The derivatives of the loss function are generally computed using an algorithmic method called \ac{AD}~\cite{doi:10.1137/1.9780898717761}.

This machinery can be used for regression analysis, where the \ac{NN} becomes a function approximator.
Say we wish to emulate a target function $f$ with a \ac{NN} $g$.
If we have a large dataset of $N$ values of the target $y_i$ at points $x_i$ such that $y_i = f(x_i)$,
\begin{align}
    \{(x_i,y_i) \,|\, i=1,\ldots,N\} \,,
\end{align}
we can use a loss function $L(\theta)$ that compares this target distribution to the output of our \ac{NN}, where $\theta$ are the parameters (weights and biases) of the \ac{NN}.
What constitutes a large $N$ depends on the number of input dimensions and the complexity of the target distribution.
A common choice is the \ac{MSE} loss function,
\begin{align}
    L(\theta) &= \frac{1}{n} \sum_i (g(x_i; \theta) - y_i)^2 \,.
\end{align}

For our problem, we do not have access to a large dataset because generating sample points with existing evaluation methods is prohibitively slow, so this approach is unviable.
However, we do know the \acp{DE} \lref{eq:deq} and can cheaply numerically sample the connection matrices $A_q$.
Therefore, we can instead train our \ac{NN} $g$ to satisfy the \acp{DE} by constructing our loss function from them with the solution $I$ substituted for the \ac{NN} $g$.
To select the correct solution, we must also specify at least one boundary point, which we can do by including them as a constraint term in our loss function.
Our loss function is detailed in \cref{sec:loss}.

% \Ac{PINN} review~\cite{cuomo2022scientific}.
% Simple description~\cite{navarro2023solving}.

\section{Model architecture}

We approximate an integral family $T=\{I_i(\vec s, \eps)\,|\,i=1,\ldots,n\}$ with a model comprising a pair of fully-connected feedforward \acp{NN}.
Each \ac{NN} is a real-valued function $g$, with one for the real parts $\re\, T$ and the other for the imaginary parts $\im\, T$ of the integrals.

The \acp{NN} have $m-1$ inputs $\vec x$, which are related to a subset of the $m$ independent kinematic variables $\vec s$.
We drop dependence on one of the variables since it can be restored analytically by dimensional analysis.
Decreasing the input dimensionality reduces the complexity of the problem.
The choice of which variable to freeze is made considering the kinematics of the particular family;
for instance, $s_{12}$ for a massless system or the mass in a system with an internal mass are natural choices.
If the last variable $s_m$ is fixed as the constant $\tilde s_m$ by the transformation,
\begin{align}
    s_i \rightarrow \tilde s_m \frac{s_{i}}{s_m} \eqqcolon x_i \qquad \forall i = 1,\ldots,m
\end{align}
then
\begin{align}
    \begin{aligned}
    I_i(s_1,\ldots,s_{m-1},s_m;\eps)
        &= \left(\frac{\tilde s_m}{s_m}\right)^{-a_i} I_i\left(x_1,\ldots,x_{m-1},x_m;\eps\right) \\
        &\eqqcolon \left(\frac{\tilde s_m}{s_m}\right)^{-a_i} f_i\left(x_1,\ldots,x_{m-1};\eps\right) \,,
    \end{aligned}
\end{align}
where $a_i$ is determined by the dimensionality of $I_i$ (which depends on $\eps$ in general) and $\{f_i\}$ are the set of functions targeted by our \ac{NN}.
Since we choose the integrals to be dimensionless, $a_i$ is always zero.
% Note that the boundary values are computed via $I_i\left(\alpha s_1,\ldots,\alpha s_{m-1},\alpha;\eps\right)$.

\Acp{NN} are highly sensitive to the range of scales involved in the problem, training optimally when all scales are of order one.
In particular, large scale variations in the distributions of the inputs and outputs can be problematic.
By judicious choice of $\tilde s_m$, we can tune these distributions with significant impact on training performance.
\RM{not if dimensionless! tune output by prefactor choice for dimension normalisation. const tunes input and DE scales}
This choice can also be exploited to shift the spurious surfaces such that the spurious cuts exclude less of the physical region.

The outputs of a \ac{NN} correspond to the real or imaginary parts of the $\eps$ coefficients of the functions $f_i^{(w)}(\vec x)$ as in \cref{eq:eps-series}.
For a family of $n$ integrals with $\nu$ orders in $\eps$ (highest weight $\nu-1$), each \ac{NN} has $n \nu$ outputs.

For the integral families we considered, we find that three hidden layers is sufficient.
We use the same width for all layers, finding around double the number of outputs rounded up to the next power of two to be suitable.
We use \ac{GELU} activation functions~\cite{hendrycks2023gaussian} on the hidden layers, with a linear output layer.

\section{Datasets}

The training dataset has two subsets: a \acp{DE} dataset and a boundary dataset.
The \acp{DE} dataset comprises a set of input points along with corresponding values of the connection matrices, $\{(x_i, A_{q}^{(k)}(x_i))\}$.
Meanwhile, the boundary dataset contains input points with values of the solutions $\{(x_b, f_j^{(w)}(x_b))\}$.
% (and optionally the derivatives $\{\vec\nabla \vec f(\vec x_0)\}$).
There is also a testing dataset $\{(x_t, f_j^{(w)}(x_t))\}$.

The \acp{DE} dataset is generated dynamically by random sampling at each iteration of the training algorithm (see \cref{sec:train}).
The boundary dataset is static and is pre-computed.

Each integral family is associated with a kinematic phase space.
The physical singularities of the system --- where the functions $f_j^{(w)}$ diverge --- lie at the boundaries of the phase space.
The \acp{DE} contain spurious singularities where entries of the connection matrices $A_{q,jr}^{(k)}$ diverge (although the derivative $\partial_q f_j^{(w)}=\sum_k \sum_r A^{(k)}_{q,jr} f^{(w-k)}_r$ is analytically finite).
These singularities occur where the denominator factors of the connection matrix entries approach zero.
When sampling points for a dataset, we regulate by satisfying cuts around the physical poles.
% \RM{The size of the cuts should be compatible with the cuts to be used at inference in production.}
For only the \acp{DE} dataset, we must also cut the spurious poles to avoid divergences in the numerical evaluation of the connection matrices.
We choose these cuts to be as small as possible while regulating the singular behaviour to minimise the absence of model exposure to these regions during training.

We parametrise the phase space of the physical scattering channel of interest in terms of energies and angles.
Then we can generate the points of the input space $x$ for \acp{DE} and testing datasets by random uniform sampling of the parametrisation.
In addition, we veto points that do not pass the appropriate singularity cuts.

While it is sufficient to specify a single boundary value, we find training performance can be improved by including additional points in the boundary dataset.
\RM{quantity}
We find that choosing points near the phase space boundaries (including cuts and spurious poles) can greatly assist training.
\RM{quantity}

\section{Loss function}
\label{sec:loss}

We define the loss function as the sum of terms for the \acp{DE} $L^{[1]}$ and the boundary values $L^{[0]}$, using \ac{MSE} for each term,
\begin{align}
    L(\theta) &= L^{[1]}(\theta) + \lambda L^{[0]}(\theta) \,, \\
    L^{[1]}(\theta) &= \frac{1}{N(m-1)n\nu} \sum_{i,q,j,w} \left(\partial_q g_j^{(w)}(x_i; \theta) - \sum_{k=0}^{\min(k_1,w)} \sum_{r=1}^{n} A_{q,jr}^{(k)}(x_i) \, g_r^{(w-k)}(x_i; \theta)\right)^2 \,, \\
    L^{[0]}(\theta) &= \frac{1}{c n \nu} \sum_{b,j,w} \left(g_j^{(w)}(x_b; \theta) - f_j^{(w)}(x_b)\right)^2 \,,
\end{align}
where $\theta$ are the parameters of the \ac{NN}, $\lambda$ is a parameter to tune the relative weight of the two terms, and $c$ is the number of pre-computed boundary values $f_j^{(w)}(x_b)$.

We observe that each \ac{NN} output $g_j^{(w)}$ is coupled to the set of coefficients
\begin{align}
    \{ g_r^{(u)} \, | \, w-\min(k_1,w)\le u\le w \, , \, 1\le r \le n \}
\end{align}
through $L^{[1]}$.
\RM{superfluous?}

To obtain the derivatives of the \ac{NN} $\partial_q g_j^{(w)}(x_i; \theta)$, we use \ac{AD}.
Note that $\partial_\theta L$ now involves a double \ac{AD}.

\RM{boundary derivatives not included}

\section{Training}
\label{sec:train}

Glorot (uniform) initialisation of weights~\cite{pmlr-v9-glorot10a}, gain one.

Initialise output biases as the mean of the boundary values.\RM{quantify improvement}

Mean training loss over epoch as training metric.

This is an unusual regime within machine learning as we have effectively an infinite training dataset.
Since the inputs are dynamically randomly sampled, regularisation is built into the training scheme.
This means methods to avoid over-fitting like L2 regularisation of the loss function or weight decay in the gradient descent algorithm are unnecessary.
We also do not use a distinct validation dataset for the same reason; the training metric is used as the validation metric too.

It's the small data/lots of physics regime~\cite{Karniadakis2021}.
``Big physics''?

The two \acp{NN} for the real and imaginary parts of an integral family are trained independently.

Implemented with \texttt{PyTorch} framework~\cite{paszke2019pytorch}.

Analogously to mini-batch training, we organise training iteration into epochs which are composed of small batches.
The difference is that every batch is a dynamic random sample of the inputs.
The choice of the batch size follows the same philosophy as in mini-batch training.
We find the optimal number grows with the number of inputs and use the heuristic $2^{4 + m}$ \RM{subject to experimentation}.
At the end of each epoch, we use the mean loss value of the batches as the metric of training performance.
We also check if the training has plateaued and reduce the learning rate if it has.
We use the \texttt{ReduceLROnPlateau} scheduler for this with parameters\ldots \RM{integral family specific but suggest general values}

Adam optimiser~\cite{kingma2017adam}. No weight decay.
Also tried RAdam~\cite{liu2021variance}, similar performance.

\section{Future directions}

Discussion of typical difficulties with PINNs~\cite{wang2023experts}.

Review of problems~\cite{hao2023physicsinformed}.

Importance sampling~\cite{Nabian_2021}.

Loss-reweighting schemes~\cite{wang2020understanding}.

% Problems in regions with large outputs: curriculum training~\cite{krishnapriyan2021characterizing}.

% Benchmarks of various PINNs~\cite{hao2023pinnacle}.

% MultiAdam~\cite{yao2023multiadam}.

\section{Conclusion}

\acknowledgments

\SZ{thank: Simon Badger, Matteo Becchetti, Ekta Chaubey, Tiziano Peraro, Johannes Henn.}
This project received funding from the European Union's Horizon 2020 research and innovation programme \textit{High precision multi-jet dynamics at the LHC} (grant agreement number 772099). \SZ{add Marie Curie fellowship}

\appendix

\section{An appendix}

\ldots

\bibliographystyle{JHEP}
\bibliography{bibliography}

\end{document}
